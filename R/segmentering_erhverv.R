#' Segmenting and scoring SNB business customers
#'
#' A custom algorithm for segmenting every business Spar Nord customer. Can only be used on a specific MART with the correct naming of variables.
#' @param x The business segmentation MART from Spar Nords Datalab
#' @export
#' @return Returns a dataframe with totalscore, potentiale, kundeindtjening, segment and transformed variables from the MART for every customer ID
#' @examples library(RODBC)
#'
#' myconn <- RODBC::odbcConnect("NZDLABPROD", uid="E-USERNAME", pwd="PASSWORD")
#'
#' root <- RODBC::sqlQuery(myconn, "select * from SEGMENT_MART")
#'
#' segmentering_erhverv(root)

segmentering_erhverv <- function(x){


  # # If a customer doesn't have a KUNDENR then use KUNDENR as KUNDENR---------------------------------------------------------------------
  # x$KUNDENR <- ifelse(is.na(x$KUNDENR), x$KUNDENR, x$KUNDENR)

  # Make sure everyone in the same household has the same HUSSTANDSPROFIL. If no HUSSTANDSPROFIL exist, then use KUNDEPROFIL
  # x <- x %>%
  #   group_by(KUNDENR) %>%
  #   mutate(HUSSTANDSPROFIL = max(HUSSTANDSPROFIL, na.rm =T)) %>%
  #   mutate(HUSSTANDSPROFIL = ifelse(is.na(HUSSTANDSPROFIL), KUNDEPROFIL, HUSSTANDSPROFIL))
  #
  # Prep TK_BELAAN_GRAD_HELAARS and TK_BELAAN_GRAD_FRITIDSHUS for use in the variable HOJT_INDLAAN_LAV_LVT-----------------------------------

  # x$TK_BELAAN_GRAD_HELAARS <- ifelse(x$TK_BELAAN_GRAD_FRITIDSHUS != 0 & !is.na(x$TK_BELAAN_GRAD_FRITIDSHUS), NA, x$TK_BELAAN_GRAD_HELAARS)
  # x$TK_BELAAN_GRAD_FRITIDSHUS <- ifelse(x$TK_BELAAN_GRAD_HELAARS != 0 & !is.na(x$TK_BELAAN_GRAD_HELAARS), NA, x$TK_BELAAN_GRAD_FRITIDSHUS)

  # Remove households only containing dead customers
  # x <- x %>%
  #   group_by(KUNDENR) %>%
  #   filter(!(sum(DOED == 1, na.rm = T) == length(KUNDENR))) %>%
  #   ungroup()

  # Recode KONTAKTPOLITIK_STATUS to 0 if NA and PRIMAER_KND from factor to numerical -------------------

  # x$KONTAKTPOLITIK_STATUS[is.na(x$KONTAKTPOLITIK_STATUS)] <- 0

  # x$PRIMAER_KND <- as.character(x$PRIMAER_KND)
  #
  # x$PRIMAER_KND[x$DOED == 1] <- -1
  # x$PRIMAER_KND[x$PRIMAER_KND == "P"] <- 2
  # x$PRIMAER_KND[is.na(x$PRIMAER_KND) | x$PRIMAER_KND == "S"] <- 0
  # x$PRIMAER_KND[x$MEDLEM_TYPE == "VOK" & x$PRIMAER_KND != 2 & x$PRIMAER_KND != -1] <- 1
  #
  # x$PRIMAER_KND <- as.numeric(x$PRIMAER_KND)
  #

  # Sum variables across households-------------------------------------------------------------------------------------------------------------

  # x$PS_IKRAFT_DATO <- as.Date(x$PS_IKRAFT_DATO)
  # x$BORNEOPSPAR_UDLOEB <- as.Date(x$BORNEOPSPAR_UDLOEB)
  #
  # x <- x %>%
  #   group_by(KUNDENR) %>%
  #   mutate(TK_TOT_OBL_RESTGAELD = sum(TK_OBL_RESTGAELD), ALM_INDLAAN = max(ALM_INDLAAN, na.rm = T), SAERL_INDLAAN = max(SAERL_INDLAAN, na.rm = T), TK_TOT_OBL_RESTGAELD = max(TK_TOT_OBL_RESTGAELD, na.rm = T), KREDIT_MAX = max(KREDIT_MAX, na.rm = T), UDLAAN = max(UDLAAN, na.rm = T), PENSION = max(PENSION, na.rm = T), STJERNEINVEST = max(STJERNEINVEST, na.rm = T), PENSION_INVESTERBAR_INDIVID = max(PENSION_INVESTERBAR, na.rm = T)) %>%
  #   ungroup() %>%
  #   group_by(KUNDENR) %>%
  #   mutate(INVESTERBAR_INDLAAN = sum(unique(INVESTERBAR_INDLAAN[TYPE_N == 100]), na.rm = T), INVESTERBAR_DEPOT = sum(unique(INVESTERBAR_DEPOT[TYPE_N == 100]), na.rm = T), PENSION_INVESTERBAR_HUS = sum(unique(PENSION_INVESTERBAR[TYPE_N == 100]), na.rm = T), ALM_INDLAAN = sum(unique(ALM_INDLAAN), na.rm = T), SAERL_INDLAAN = sum(unique(SAERL_INDLAAN), na.rm = T), TK_TOT_OBL_RESTGAELD = sum(unique(TK_TOT_OBL_RESTGAELD), na.rm = T), KREDIT_MAX = sum(unique(KREDIT_MAX), na.rm = T), UDLAAN = sum(unique(UDLAAN), na.rm = T), PENSION = sum(unique(PENSION), na.rm = T), STJERNEINVEST = sum(unique(STJERNEINVEST), na.rm = T), SCORING_RATING = max(SCORING_RATING, na.rm = T), TK_EJERBOLIG_KOEB = max(TK_EJERBOLIG_KOEB, na.rm = T), SENESTE_PENSIONSINFO = max(SENESTE_PENSIONSINFO, na.rm = T), SENESTE_MOEDE = max(SENESTE_MOEDE, na.rm = T), SENESTE_KONTAKT = max(SENESTE_KONTAKT, na.rm = T), INDLAAN_PRIORITET = sum(unique(INDLAAN_PRIORITET), na.rm = T), INDLAAN_LOENKONTI = sum(unique(INDLAAN_LOENKONTI), na.rm = T), UUDNYTTET_KREDIT = sum(unique(UUDNYTTET_KREDIT), na.rm = T), BOLIGUDBUDT = max(BOLIGUDBUDT, na.rm = T), TK_SENESTE_REFIN_LAAN_DATO = max(TK_SENESTE_REFIN_LAAN_DATO, na.rm = T), BIL_INDREG_KOEBS_DATO = min(BIL_INDREG_KOEBS_DATO, na.rm = T), OEI = max(OEI, na.rm = T), EJERPANTEBREV = max(EJERPANTEBREV, na.rm = T), UDENLANDS = max(UDENLANDS, na.rm = T), FLYT_ENGAGEMENT = max(FLYT_ENGAGEMENT, na.rm = T), RKI_MARKERING = max(RKI_MARKERING, na.rm = T), KONTAKTPOLITIK_STATUS = max(KONTAKTPOLITIK_STATUS, na.rm = T), KONTAKTPOLITIK_BEHANDLET_DATO = max(KONTAKTPOLITIK_BEHANDLET_DATO, na.rm = T), KONTAKTPOLITIK_BEHANDLET_AF = max(KONTAKTPOLITIK_BEHANDLET_AF, na.rm = T), KND_INDTJ = max(KND_INDTJ, na.rm = T), PRIORITETSKONTI_OPRETTET = max(PRIORITETSKONTI_OPRETTET, na.rm = T)) %>%
  #   ungroup()

  # # Remove a customer/household if they have FLYT_ENGAGEMENT
  # x <- x %>%
  #   filter(is.na(FLYT_ENGAGEMENT))

  # Transform SENESTE_MOEDE and SENESTE_KONTAKT into numeric variable with number of days since last contact with the customer and spread across the entire household-------------------------------------------------------------------------------------------------------------------------------------------------

  # x$SENESTE_MOEDE_NUM <- as.numeric(Sys.Date() - x$SENESTE_MOEDE)
  # x$SENESTE_KONTAKT_NUM <- as.numeric(Sys.Date() - x$SENESTE_KONTAKT)

  # Transform variables into binary variables-------------------------------------------------------------------------------------------------------------

  # x <- x %>%
  #   group_by(KUNDENR) %>%
  #   mutate(TK_REFINAN = min(TK_REFINAN), TK_AFDRAGSFRI_UDLOEBER = min(TK_AFDRAGSFRI_UDLOEBER)) %>%
  #   ungroup() # spread min TK_REFINAN and TK_AFDRAGSFRI_UDLOEBER across household

  # Change all variables with datatype POSIXT into DATE format in order to perform mathematic operations on DATE variables

  for (i in 1:length(x)) {

    if (is.POSIXt(x[, i])) {

      x[,i] <- as_Date(x[,i])
    }

  }

  #Prep TK_LAANETYPE to a string instead of a factor

  x$TK_LAANETYPE <- as.character(x$TK_LAANETYPE)

  #Prep DLR_LAANETYPE to a string instead of a factor

  x$DLR_LAANETYPE <- as.character(x$DLR_LAANETYPE)

  # Calculate whether afdragsfrihed expires within the next 12 months

  x$TK_AFDRAGSFRI_UDLOEBER <- ifelse(x$TK_AFDRAGSFRI_UDLOEBER == '0001-01-01' | x$TK_AFDRAGSFRI_UDLOEBER < Sys.Date(), NA, ifelse(x$TK_AFDRAGSFRI_UDLOEBER - Sys.Date() < 30.44 * 12 & x$TK_OBL_RESTGAELD != 0, 1, 0))

  # Calculate whether afdragsfrihed expires within the next 12 months

  x$DLR_AFDRAGSFRI_UDLOEBER <- ifelse(x$DLR_UDLOEB_AFDRAGFRI_DATO == '0001-01-01' | x$DLR_UDLOEB_AFDRAGFRI_DATO < Sys.Date(), NA, ifelse(x$DLR_UDLOEB_AFDRAGFRI_DATO - Sys.Date() < 30.44 * 12 & x$DLR_OBLIGA_REST_GAELD != 0, 1, 0))

  #Calculate the number of TK loan nr. per building nr
  x <- x %>%
    group_by(TK_XP_EJENDOMSNR) %>%
    mutate(TK_XP_LANENR_COUNT = length(unique(na.omit(TK_XP_LANELQBENR[TK_OBL_RESTGAELD != 0])))) %>%
    ungroup()

  #Calculate the number of DLR loan nr. per building nr
  x <- x %>%
    group_by(DLR_BBRNR) %>%
    mutate(DLR_BBRNR_COUNT = length(unique(na.omit(DLR_SAGSNR[DLR_OBLIGA_REST_GAELD != 0])))) %>%
    ungroup()

  # Potentiale 1 - Indlaan/formue/vaerdipapirer - Erhverv
  x$INDLAAN_U_STJ_INVEST <- ifelse(x$DIFF_LIKVIDE_MIDLER < -200000, 1, 0)

  # Potentiale 2 - Formue - Erhverv
  x$FORMUE_O_200 <- ifelse(rowSums(x[,c("KTO_LIKVIDEMIDLER", "DEP_VAERDIPAPIRER")], na.rm=TRUE) >= 200000, 1, 0)

  # Potentiale 3 - Realkredit - Erhverv
  x$REALKREDIT_POT <- ifelse(x$DIFF_REALKREDIT < 0, 1, 0)

  # Potentiale 4 - Udlaan - Erhverv
  x$UDLAAN_POT <- ifelse(x$DIFF_UDLAAN < -100000, 1, 0)

  # Potentiale 5 - Leasing - regnskab afslører et potentiale
  x$LEASING_ANDRE <- ifelse(x$DIFF_LEASING < 0, 1, 0)

  # Potentiale 6 - Leasing - betalinger til andre selskaber
  x$LEASING_POT <- ifelse(x$POST_LEASING == 'J' & !(x$KTO_LEASING > 0), 1, 0)

  # Potentiale 7 - Totalkredit/DLR - konverteringspotentiale for kunden
  x$TK_POTENTIALE <- ifelse(x$TK_OBL_RENTEPCT >= 2.5 & x$TK_AFDRAGSFRI_MARK == "Nej" & Sys.Date() - as.Date(x$TK_LAAN_UDBETAL_DATO) >= 30.44 * 18 & x$TK_LAANETYPE == "AF" & x$TK_RESTLOEBETID >= 20, 1,
                            ifelse(x$TK_OBL_RENTEPCT == 2 & x$TK_AFDRAGSFRI_MARK == "Nej" & Sys.Date() - as.Date(x$TK_LAAN_UDBETAL_DATO) >= 30.44 * 18 & x$TK_LAANETYPE == "AF" & x$TK_RESTLOEBETID >= 20, 1,
                                   ifelse(x$TK_OBL_RENTEPCT >= 2 & x$TK_AFDRAGSFRI_MARK == "Ja" & Sys.Date() - as.Date(x$TK_LAAN_UDBETAL_DATO) >= 30.44 * 18 & x$TK_AFDRAGSFRI_UDLOEBER == 1 & x$TK_LAANETYPE == "AF" & x$TK_RESTLOEBETID >= 20, 1,
                                          ifelse(x$TK_OBL_RENTEPCT >= 2 & Sys.Date() - as.Date(x$TK_LAAN_UDBETAL_DATO) >= 30.44 * 18  & x$TK_LAANETYPE == "AF" & x$TK_RESTLOEBETID >= 10 & x$TK_RESTLOEBETID <= 20, 1,
                                                 ifelse(x$TK_OBL_RENTEPCT == 1.5 & Sys.Date() - as.Date(x$TK_LAAN_UDBETAL_DATO) >= 30.44 * 18  & x$TK_LAANETYPE == "AF" & x$TK_RESTLOEBETID >= 10 & x$TK_RESTLOEBETID <= 20, 1,
                                                        ifelse(Sys.Date() - as.Date(x$TK_LAAN_UDBETAL_DATO) >= 30.44 * 18  & x$TK_LAANETYPE %in% c("C3", "FK", "XR", "XV"), 1,
                                                               ifelse(Sys.Date() - as.Date(x$TK_LAAN_UDBETAL_DATO) >= 30.44 * 18  & x$TK_XP_LANENR_COUNT >= 2, 1, 0

                                                               )

                                                        )

                                                 )

                                          )

                                   )

                            )


  )

x$DLR_POTENTIALE <- ifelse(x$DLR_KUPONRENTE >= 2.5 & x$DLR_AFDRAGSFRI_MARK == "Nej" & Sys.Date() - as.Date(x$DLR_UDBETALINGSDATO) >= 30.44 * 18 & x$DLR_REFINANSIERINGDATO == "0001-01-01" & x$DLR_RESTLOEBE_TID_AAR >= 20, 1,
         ifelse(x$DLR_KUPONRENTE == 2 & x$DLR_AFDRAGSFRI_MARK == "Nej" & Sys.Date() - as.Date(x$DLR_UDBETALINGSDATO) >= 30.44 * 18 & x$DLR_REFINANSIERINGDATO == "0001-01-01" & x$DLR_RESTLOEBE_TID_AAR >= 20, 1,
                ifelse(x$DLR_KUPONRENTE >= 2 & x$DLR_AFDRAGSFRI_MARK == "Ja" & Sys.Date() - as.Date(x$DLR_UDBETALINGSDATO) >= 30.44 * 18 & x$DLR_AFDRAGSFRI_UDLOEBER == 1 & x$DLR_REFINANSIERINGDATO == "0001-01-01" & x$DLR_RESTLOEBE_TID_AAR >= 20, 1,
                       ifelse(x$DLR_KUPONRENTE >= 2 & Sys.Date() - as.Date(x$DLR_UDBETALINGSDATO) >= 30.44 * 18  & x$DLR_REFINANSIERINGDATO == "0001-01-01" & x$DLR_RESTLOEBE_TID_AAR >= 10 & x$DLR_RESTLOEBE_TID_AAR <= 20, 1,
                              ifelse(x$DLR_KUPONRENTE == 1.5 & Sys.Date() - as.Date(x$DLR_UDBETALINGSDATO) >= 30.44 * 18  & x$DLR_REFINANSIERINGDATO == "0001-01-01" & x$DLR_RESTLOEBE_TID_AAR >= 10 & x$DLR_RESTLOEBE_TID_AAR <= 20, 1,
                                     ifelse(Sys.Date() - as.Date(x$DLR_UDBETALINGSDATO) >= 30.44 * 18  & x$DLR_REFINANSIERINGDATO != "0001-01-01", 1,
                                            ifelse(Sys.Date() - as.Date(x$DLR_UDBETALINGSDATO) >= 30.44 * 18  & x$DLR_BBRNR_COUNT >= 2, 1, 0

                                            )

                                     )

                              )

                       )

                )

         )


  )

# Merge the two credit types into one RK_POTENTIALE
x$RK_POTENTIALE <- ifelse(x$TK_POTENTIALE == 1 | x$DLR_POTENTIALE == 1, 1, 0)

  # Potentiale 8 - Refinansiering - Snarlig refinansiering af kunden realkredit lån
  x$RK_REFINAN_POT <- ifelse(Sys.Date() - as.Date(x$TK_LAAN_UDBETAL_DATO) >= 30.44 * 18  & x$TK_LAANETYPE %in% c("C3", "FK", "XR", "XV") & x$TK_REFINAN == "2019-12-30", 1, ifelse(Sys.Date() - as.Date(x$DLR_UDBETALINGSDATO) >= 30.44 * 18  & x$DLR_REFINANSIERINGDATO != "0001-01-01" & x$DLR_REFINANSIERINGDATO == "2020-01-01", 1, 0))

  # Potentiale 9 - Privatsikring Erhverv
  x$PRIVSIK_ERHV <- ifelse(is.na(x$PS_IKRAFT_DATO) & x$PRIVATSIKRING_BRANCHE == "J", 1, 0)

  # Potentiale 10 - Privatsikring Erhverv - ejer har private forsikringer i Privatsikring
  x$PRIVSIK_EJER <- ifelse(x$PS_P145_ANTAL > 0 & x$PRIVATSIKRING_BRANCHE == "J", 1, 0)

  # Potentiale 11 - Nærpension
  x$NAERPENS <- ifelse(!(x$NAERPENSION_EMPLOYEES %in% c(NA, 1)), 1, 0)

  # Potentiale 12 - Gældspleje (andelsboligforening)
  x$GAELDSPLEJE_ANDELSFOR <- ifelse(x$GAELDSPLEJE_AB_LANGREALKREDIT >= 5000000 & x$SCORING_RATING <= 5, 1, 0)

  # Potentiale 13 - Gældspleje (udlån)
  x$GAELDSPLEJE_BANKUDL <- ifelse(x$GAELDSPLEJE_BANKUDLAAN >= 7500000 & x$SCORING_RATING <= 5, 1, 0)

  # Potentiale 14 - Gældspleje (realkredit)
  x$GAELDSPLEJE_REALKRD <- ifelse(x$GAELDSPLEJE_REALKREDIT >= 7500000 & x$SCORING_RATING <= 5, 1, 0)

  # Potentiale 15 - Gældspleje (Leasing)
  x$GAELDSPLEJE_LEAS <- ifelse(x$GAELDSPLEJE_LEASING >= 5000000 & x$SCORING_RATING <= 5, 1, 0)

  # Potentiale 16 - Udenlandske overførsler
  x$UDL_OVERFORS <- ifelse(x$UDL_OVF_AAR_BELOEB > 10000000, 1, 0)

  # Potentiale 17 - Ejer af enkeltmandsvirksomheder uden pensionsprodukter i SNB
  x$EJER_UDEN_PENSION <- ifelse(!is.na(x$ENV_REEL_EJER_UDEN_PENSION), 1, 0)

  # Potentiale 18
  x$REGN_LANG_GAELD_POT <- ifelse(!is.na(x$REGN_LANG_GAELD), 1, 0)




  # Set NAs to zero for certain variables in order to avoid addition with NAs--------------------------------------------------------------------------

  x <- as.data.frame(x) # Set to data.frame instead of tbl_df in order for the loop to work

  x$BANKST_NAVN_T <- as.character(x$BANKST_NAVN_T)
  x$KUNDEANSV_NAVN_T <- as.character(x$KUNDEANSV_NAVN_T)
  x$KUNDEANSV_E_BRUGER <- as.character(x$KUNDEANSV_E_BRUGER)
  # x$MEDLEM_TYPE <- as.character(x$MEDLEM_TYPE)

  for (i in 1:length(x)) {

    if (is.numeric(x[ , i]) | is.integer(x[ , i]) | is.factor(x[ , i]) | is.logical(x[ , i])) {

      x[ , i] <- ifelse(is.na(x[ , i]), 0, x[ , i])
    }
  }


  # Group all the variables to be used in the weighted potential score across the company-----------------------------------------------------------

  x <- x %>%
    group_by(KUNDENR) %>%
    mutate(LEASING_POT = max(LEASING_POT, na.rm = T), LEASING_ANDRE = max(LEASING_ANDRE, na.rm = T), UDLAAN_POT = max(UDLAAN_POT, na.rm = T), INDLAAN_U_STJ_INVEST = max(INDLAAN_U_STJ_INVEST, na.rm = T), FORMUE_O_200 = max(FORMUE_O_200, na.rm = T), REALKREDIT_POT = max(REALKREDIT_POT, na.rm = T), RK_POTENTIALE = max(RK_POTENTIALE, na.rm = T),  PRIVSIK_ERHV = max(PRIVSIK_ERHV, na.rm = T), PRIVSIK_EJER = max(PRIVSIK_EJER, na.rm = T), NAERPENS = max(NAERPENS, na.rm = T), GAELDSPLEJE_ANDELSFOR = max(GAELDSPLEJE_ANDELSFOR, na.rm = T), GAELDSPLEJE_BANKUDL = max(GAELDSPLEJE_BANKUDL, na.rm = T), GAELDSPLEJE_REALKRD = max(GAELDSPLEJE_REALKRD, na.rm = T), GAELDSPLEJE_LEAS = max(GAELDSPLEJE_LEAS, na.rm = T), UDL_OVERFORS = max(UDL_OVERFORS, na.rm = T), EJER_UDEN_PENSION = max(EJER_UDEN_PENSION, na.rm = T), REGN_LANG_GAELD_POT = max(REGN_LANG_GAELD_POT, na.rm = T) ) %>%
    ungroup()

  # Calculate the BASE_SCORE-----------------------------------------------------------------------------------------------------------------
  x <- x %>%
    group_by(KUNDENR) %>%
    mutate(BASE_SCORE = max(KTO_LIKVIDEMIDLER, na.rm = T) + max(DEP_VAERDIPAPIRER, na.rm = T) + max(KTO_LEASING, na.rm = T) + max(KTO_UDLAAN, na.rm = T))

  # Rescale BASE_SCORE to 0-1
  x$BASE_SCORE <- (x$BASE_SCORE - min(x$BASE_SCORE, na.rm = T)) / (max(x$BASE_SCORE, na.rm = T) - min(x$BASE_SCORE, na.rm = T))


  # Calculate the weighted potential score  -----------------

  x <- x %>%
    group_by(KUNDENR) %>%
    arrange(desc(PRIMAER_KND)) %>%
    distinct(KUNDENR, .keep_all = T) %>%
    mutate(A = sum(UDLAAN_POT, LEASING_ANDRE, RK_POTENTIALE,  na.rm=T) * (5/16)+
             sum(0, na.rm = T) * (4/16) +
             sum(LEASING_POT, FORMUE_O_200, INDLAAN_U_STJ_INVEST, REALKREDIT_POT, PRIVSIK_ERHV, PRIVSIK_EJER, NAERPENS, GAELDSPLEJE_ANDELSFOR, GAELDSPLEJE_BANKUDL, GAELDSPLEJE_REALKRD, GAELDSPLEJE_LEAS, UDL_OVERFORS, EJER_UDEN_PENSION, REGN_LANG_GAELD_POT, na.rm = T) * (3/16) +
             sum(0, na.rm = T) * (2/16) +
             sum(0 * (1/16)) +
             BASE_SCORE * (0.2/16)) %>%
    ungroup()

  # Rescale score to 0-100
  x$A <- ((x$A - min(x$A)) / (max(x$A) - min(x$A))) * 100

  # If a customer has no potential, then append the BASE_SCORE as potential
  x$C <- rank(-x$A)
  x[x$A == 0,"C"] <- sort.list(order(-x[x$A == 0,"BASE_SCORE"]))+length(which(!x$A == 0))

  x$A[x$A == 0] <- NA # Zeroes are transformed to NAs to ignore these cases in the calculations below

# x$POTENTIALE <- ifelse(x$OEI == 1 | x$RKI_MARKERING == 1 | x$SCORING_RATING == 11 | (x$UDENLANDS == 1 & x$ALM_INDLAAN < 200000 ), 0 ,
#                          ifelse(is.na(x$A), x$BASE_SCORE / min(x$A, na.rm = T) / x$C, x$A))

  x$POTENTIALE <- ifelse(is.na(x$A), x$BASE_SCORE / min(x$A, na.rm = T) / x$C, x$A)

  # Transform POTENTIALE to a  rank variable and rescale to 0-100---------------------------------------------------------------------------------------
  x$POTENTIALE <- rank(x$POTENTIALE, ties.method = 'min')
  x$POTENTIALE <- rescale(x$POTENTIALE, to = c(0,100))

  # Rescale the x-axis, KND_INDTJ to 0-100---------------------------------------------------------------------------------------------------

  x$KND_INDTJ <- ((x$KND_INDTJ - min(x$KND_INDTJ, na.rm = T)) / (max(x$KND_INDTJ, na.rm = T) - min(x$KND_INDTJ, na.rm = T))) * 100

  # Transform KND_INDTJ to a rank variable and rescale to 0-100---------------------------------------------------------------------------
  x$KND_INDTJ <- rank(x$KND_INDTJ, ties.method = 'min')
  x$KND_INDTJ <- rescale(x$KND_INDTJ, to = c(0,100))

  # Calculate which segment a customer belongs to based on KND_INDTJ and potential----------------------------------------------------------------------

  x$SEGMENT <- ifelse(x$POTENTIALE > -0.0000000000000000000000000000000000000000000000000006 * x$KND_INDTJ^27 - 0.000005 * x$KND_INDTJ + 90, 1,
                      ifelse(x$POTENTIALE > -0.0000000000000000000000000000000000000000000000002 *  x$KND_INDTJ^27 - 0.000005 *  x$KND_INDTJ + 72, 2,
                             ifelse(x$POTENTIALE > -0.00000000000000000000000000000000000000000059 * x$KND_INDTJ^27 - 0.000005 * x$KND_INDTJ + 46, 3, 4)))

  # Calculate whether a customer is leaning towards Sales or Maintenance--------------------------------------------------------------------------------
  x$SEGMENT_VAEGT <- ifelse(2 * x$KND_INDTJ + 0 < x$POTENTIALE, 'S',
                            ifelse(0.5 * x$KND_INDTJ + 0 > x$POTENTIALE, 'V', NA))

  # If segment is 4 then don't apply a lean towards sales og maintenance
  x$SEGMENT_VAEGT <- ifelse(x$SEGMENT == 4, NA, x$SEGMENT_VAEGT)

  # Inforcing quarantine rules - if a customer have been contacted with the interval for his/her segment prescribed by the contact policy, then potentiale and KND_INDTJ is set to NA------------------------------------------------------------------------------------------------------------------------------

  # x$POTENTIALE <- ifelse(x$SEGMENT == 1 & x$SENESTE_MOEDE_NUM > 0 & x$SENESTE_MOEDE_NUM < 30.44 * 6, NA, x$POTENTIALE) # Contact policy for segment 1 is every 6 months
  # x$KND_INDTJ <- ifelse(x$SEGMENT == 1 & x$SENESTE_MOEDE_NUM > 0 & x$SENESTE_MOEDE_NUM < 30.44 * 6, NA, x$KND_INDTJ) # Contact policy for segment 1 is every 6 months
  #
  # x$POTENTIALE <- ifelse(x$SEGMENT == 2 & x$SENESTE_MOEDE_NUM > 0 & x$SENESTE_MOEDE_NUM < 365.25, NA, x$POTENTIALE) # Contact policy for segment 2 is every 12 months
  # x$KND_INDTJ <- ifelse(x$SEGMENT == 2 & x$SENESTE_MOEDE_NUM > 0 & x$SENESTE_MOEDE_NUM < 365.25, NA, x$KND_INDTJ) # Contact policy for segment 2 is every 12 months
  #
  # x$POTENTIALE <- ifelse(x$SEGMENT == 3 & x$SENESTE_MOEDE_NUM > 0 & x$SENESTE_MOEDE_NUM < 365.25 * 1.5, NA, x$POTENTIALE) # Contact policy for segment 3 is every 18 months
  # x$KND_INDTJ <- ifelse(x$SEGMENT == 3 & x$SENESTE_MOEDE_NUM > 0 & x$SENESTE_MOEDE_NUM < 365.25 * 1.5, NA, x$KND_INDTJ) # Contact policy for segment 3 is every 18 months
  #
  # x$POTENTIALE <- ifelse(x$SEGMENT == 4, NA, x$POTENTIALE) # Contact policy for segment 4 is no personal contact
  # x$KND_INDTJ <- ifelse(x$SEGMENT == 4, NA, x$KND_INDTJ) # Contact policy for segment 4 is no personal contact

  # Calculate the combined score aggregating KND_INDTJ and potentiale-----------------------------------------------------------------------------------
  x$TOTAL_SCORE <- x$KND_INDTJ + x$POTENTIALE

  # Create RANK variable which determines how the households are sorted and prioritized in Hitlisten.
  x$RANK <- order(order(rank(-x$SEGMENT, -x$TOTAL_SCORE, ties.method = "min"), x$TOTAL_SCORE))

  # Rescale RANK to 0-100
  x$RANK <- rescale(x$RANK, to = c(0,100))

  # If POTENTIALE = NA & KND_INDTJ = NA then set RANK to NA
  x$RANK <- ifelse(is.na(x$POTENTIALE) & is.na(x$KND_INDTJ), NA, x$RANK)

  # If KONTAKTPOLITIK_STATUS == 1 (Behandlet) then set RANK to NA
  # x$RANK <- ifelse(x$KONTAKTPOLITIK_STATUS == 1 & Sys.Date() - x$KONTAKTPOLITIK_BEHANDLET_DATO < 365.25 * ifelse(x$SEGMENT == 1, 0.5,
  #                                                                                                                ifelse(x$SEGMENT == 2, 1,
  #                                                                                                                       ifelse(x$SEGMENT == 3, 1.5, NA ))), NA, x$RANK)

  # Gather x, score and new KND_INDTJ, HUSSTAND_NY_VOKsen, husstand_ny_barn, husstand_ny_reduceret and sidste_kontakt in a data frame
  final <- x

  return(final)

}
